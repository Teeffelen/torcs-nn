package sandwich;

import org.encog.Encog;
import org.encog.neural.networks.BasicNetwork;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EvolveNetwork {
	/* Constructor */
	public EvolveNetwork() {}

	/**
	 * Evolve a BasicNetwork
	 * <p>
	 * Evolve a BasicNetwork to improve it's performace on the track
	 *
	 * @param network BasicNetwork to evolve
	 * @param trackName name of the track to evolve on
	 * @return evolved BasicNetwork
	 */
	public BasicNetwork evolve(BasicNetwork network, String trackName) {
		/* TODO: run race() with network */
		/* TODO: store run times */

		/* TODO: mutate network slightly */

		/* TODO: run race() again with mutated network */
		/* TODO: store run times */

		/* TODO: compaire run times */

		/* TODO: if mutated is better/faster, combine it */
		/* TODO: else evolve again (start over) */
		return (BasicNetwork) network;
	}

	/**
	 * Race with BasicNetwork
	 * <p>
	 * Race with a BasicNetwork on a track and get the lap time total.
	 *
	 * @param network BasicNetwork to race with
	 * @param trackName name of the track to race on
	 * @return lap time
	 */
	public double race(BasicNetwork network, String trackName) {
		/* TODO: setup a driver with <network> */
		/* TODO: setup a race on <track> */

		/* TODO: run a race */

		/* TODO: get time and return it */
		double time = 0;
		return time;
	}

	/**
	 * Mutate a BasicNetwork
	 * <p>
	 * Change the weights from a BasicNetwork slightly.
	 *
	 * @param a current BasicNetwork to mutate
	 * @return evolved BasicNetwork
	 */
	public BasicNetwork mutate(BasicNetwork a) {
		/* TODO: Change all (or some?) weights slightly */
		return a;
	}

	/**
	 * Combine two BasicNetwork objects
	 * <p>
	 * When an evolution is better than the previous one,
	 * store their combined weights into a new BasicNetwork.
	 *
	 * @param a first BasicNetwork to combine with b
	 * @param b second BasicNetwork to combine with a
	 * @return combined BasicNetwork
	 */
	public static BasicNetwork combine(BasicNetwork a, BasicNetwork b) {
		BasicNetwork combined = (BasicNetwork) a.clone();
		int hiddenNodes = 19;

		/* Loop through all connections (hidden nodes --> output nodes) */
		for (int from = 0; from < hiddenNodes; ++from) {
			for (int to = 0; to < 3; ++to) {
				double a_value = a.getWeight(1, from, to);
				double b_value = b.getWeight(1, from, to);

				/* Set a combined weight for the new network */
				combined.setWeight(1, from, to, (a_value + b_value) / 2);
			}
		}
		return combined;
	}
}
