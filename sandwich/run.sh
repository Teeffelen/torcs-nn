#!/bin/sh
# Compiles the source code into a executable class files

# Clean old files
rm -f ./classes/sandwich/*.class

# Compile with jar file
javac -d "./classes" -cp '.:lib/encog-core-3.4.jar' ./*.java

# Run code with jar file
cd classes
java -cp '.:sandwich/encog-core-3.4.jar' sandwich.Client
