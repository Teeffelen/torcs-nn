package sandwich;

import org.encog.Encog;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.util.arrayutil.NormalizationAction;
import org.encog.util.arrayutil.NormalizedField;
import org.encog.util.csv.CSVFormat;
import org.encog.util.simple.TrainingSetUtil;

import java.io.*;

public class NeuralNetwork implements Serializable {
	/* DEBUG mode toggle */
	private boolean DEBUG = false;

	/* Local field */
	private BasicNetwork network;

	/**
	 * Constructor
	 * <p>
	 * Used to create a NeuralNetwork without a BasicNetwork.
	 */
	NeuralNetwork() { System.out.println("Made empty network"); }

	/**
	 * Constructor
	 * <p>
	 * Used to create a NeuralNetwork with a new BasicNetwork (Random weights).
	 *
	 * @param inputs number of nodes in the input layer
	 * @param hidden1 number of nodes in the first hidden layer
	 * @param hidden2 number of nodes in the second hidden layer
	 * @param outputs number of nodes in the output layer
	 */
	NeuralNetwork(int inputs, int hidden1, int hidden2, int outputs) {
		network = new BasicNetwork(); // New network
		network.addLayer(new BasicLayer(null, true, inputs)); // Input layer

		/* Hidden layer(s) */
		network.addLayer(new BasicLayer(new ActivationTANH(), true, hidden1));
		if (hidden2 > 0)
			network.addLayer(new BasicLayer(new ActivationTANH(), true, hidden2));
		// TODO: check if bias nodes actually improve driving

		/* Output layer */
		network.addLayer(new BasicLayer(new ActivationTANH(), false, outputs));

		network.getStructure().finalizeStructure(); // Construct Network
		network.reset(); // Set random weights

		System.out.println("Made new network");
	}

	/**
	 * Training
	 * <p>
	 * Used to train a BasicNetwork with a training file (CSV).
	 *
	 * @see <a href="http://heatonresearch-site.s3-website-us-east-1.amazonaws.com/javadoc/encog-3.3/overview-summary.html">Encog documentation (Java)</a>
	 *
	 * @param epochs number of epochs for training
	 * @param trainFile filepatch to training file (CSV)
	 */
	public void train(int epochs, String trainFile) {
		System.out.println("Training: " + trainFile);
		final MLDataSet trainingSet = TrainingSetUtil.loadCSVTOMemory(CSVFormat.ENGLISH, trainFile, true, 22, 3);

		int epoch = 1; // Start at iteration 1
		final MLTrain train = new ResilientPropagation(network, trainingSet);
		do {
			train.iteration(); // Iterate
			if (epoch % 100 == 0) // Print error every 100 epochs
				System.out.println("Epoch #" + epoch + "\tError: " + train.getError());

			epoch++;
		} while (epoch < epochs);

		/* Finish training and print result */
		train.finishTraining();
		System.out.println("Network training error: " + network.calculateError(trainingSet));
	}

	/**
	 * Compute output
	 * <p>
	 * Used to compute outputs with the sensor inputs using the neural network.
	 *
	 * @see <a href="http://heatonresearch-site.s3-website-us-east-1.amazonaws.com/javadoc/encog-3.3/overview-summary.html">Encog documentation (Java)</a>
	 *
	 * @param sensors SensorModel object contraining Torcs sensors
	 * @return [accelerate, brake, steering]
	 */
	public double[] getOutput(SensorModel sensors) {
		/* Sensor fields used for normalizing */
		NormalizedField speed = new NormalizedField(
			NormalizationAction.Normalize, "speed", 400, -400, 1.0, -1.0);
		NormalizedField pos = new NormalizedField(
			NormalizationAction.Normalize, "trackPosition", 200, -200, 1.0, -1.0);
		NormalizedField angle = new NormalizedField(
			NormalizationAction.Normalize, "angleToTrackAxis", Math.PI, -Math.PI, 1.0, -1.0);
		NormalizedField edge = new NormalizedField(
			NormalizationAction.Normalize, "edge_sensor", 200, 0, 1.0, -1.0);

		/* Normalize sensordata and add to input array */
		double[] input = new double[22];
		input[0] = speed.normalize(sensors.getSpeed());
		input[1] = pos.normalize(sensors.getTrackPosition());
		input[2] = angle.normalize(sensors.getAngleToTrackAxis());
		for (int i = 0; i < 19; i++) // Add the 19 TrackEdgeSensors
			input[i+3] = edge.normalize(sensors.getTrackEdgeSensors()[i]);

		/* Print normalized values, used for debugging */
		if (DEBUG) {
			for (int i = 0; i < input.length; ++i)
				System.out.println(i + ": " + input[i]);
			System.out.println("");
		}

		double[] output = new double[3]; // Output array
		network.compute(input, output); // Compute to output array
		return output; // Return output
	}

	/**
	 * Store BasicNetwork
	 * <p>
	 * Used to store the current network object (BasicNetwork) to a file.
	 *
	 * @param memoryFile file to which a BasicNetwork is stored
	 */
	public void storeGenome(String memoryFile) {
		System.out.println("Saving memory to: " + memoryFile);
		ObjectOutputStream out = null;
		try {
			/* TODO create the memory folder automatically */
			/* Try to store the current BasicNetwork to a file */
			out = new ObjectOutputStream(new FileOutputStream(memoryFile));
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (out != null) {
				out.writeObject(network);
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load BasicNetwork
	 * <p>
	 * Used to load the current network object (BasicNetwork) from a file.
	 *
	 * @param memoryFile file from which a BasicNetwork is loaded
	 * @return false if load was unsuccessful
	 */
	public boolean loadGenome(String memoryFile) {
		System.out.println("Loading memory: " + memoryFile);
		// Read from disk using FileInputStream
		FileInputStream file = null;
		try {
			file = new FileInputStream(memoryFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Read object using ObjectInputStream
		ObjectInputStream object = null;
		try {
			object = new ObjectInputStream(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Read an object
		try {
			if (object != null) {
				// Try to load a BasicNetwork from file
				network = (BasicNetwork) object.readObject();
				return true;
			}
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	/* terminate neural network */
	public void kill() { Encog.getInstance().shutdown(); }
}
