#!/bin/sh
# Compiles the source code into a executable jar file

# Clean old jar file
rm -f ./jar/sandwich.jar

# Compile source files with encog
javac -d "./jar" -cp '.:lib/encog-core-3.4.jar' ./*.java

# Extract Encog if not exist already
if [ ! -f ./jar/org/encog/Encog.class ]; then
	cd ./lib
	jar xf encog-core-3.4.jar
	rm -r ./META-INF
	mv ./org ../jar
	cd ..
fi

# Compress into jar
cd ./jar
jar cMf sandwich.jar META-INF memory sandwich org

# try to execute jar file
java -jar sandwich.jar port:3001
