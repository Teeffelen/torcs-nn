# Team Sticky Sandwich
The [online javadoc](https://teeffelen.gitlab.io/torcs-nn) of the source code is available.


## Running directly

To run the program directly, execute the `run.sh` file:
```
$ ./run.sh
Starting Client
Made empty network
Loading memory: ./memory/memory.mem
Ready to race!
```
> **Note:** it is possible to edit the `run.sh` script to configure the client, e.g. to run on port 3002 instead of the default 3001.


## Compiling as JAR

To compile the program to a compressed .jar file, execute the `jar.sh` file:
```
$ ./jar.sh
Starting Client
Made empty network
Loading memory: ./memory/memory.mem
Ready to race!
```
> **Note:** this script will try to execute the jar file, as seen above, for devolpment purposes.

> **Note:** this script will use the `memory.mem` file found in `sandwich/jar/memory/memory.mem`, if you made changes to the neural network I'd suggest copying the new memory file from `sandwich/classes/memory/memory.mem` to the `sandwich/jar/memory/memory.mem` directory before exporting as JAR.

The exported JAR file can be found in `sandwich/jar/sandwich.jar`.


## Configure neural network

Configuration of the neural network can be found in the `neuralDriver` class:

```java
public NeuralDriver() {
	boolean newNetwork	= false; // Create new network
	boolean training	= false; // Train network
```

This next line decides the number of input, hidden and ouput neurons that should be used. If hidden layer two has 0 neurons it will not be enabled when the network is initialized.

```java
neuralNetwork = new NeuralNetwork(22, 19, 0, 3); // Create new network
```

Training can be done be setting `training` to `true`, the training file and number of epochs can be configure with:

```java
neuralNetwork.train(7000, "../training/normalized/combined_data.csv");
```
